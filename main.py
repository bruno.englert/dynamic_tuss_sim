import math

import numpy as np
import scipy
import scipy.linalg

from utils.create_bridge import create_bridge
from utils.get_stiffness_mtx import get_stiffness_mtx

import arcade

# Set up the constants
SCREEN_WIDTH = 1200
SCREEN_HEIGHT = 600


class Line:
    """ Class to represent a rectangle on the screen """

    def __init__(self, x0, y0, x1, y1):
        """ Initialize our rectangle variables """

        # Position
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1

    def draw(self):
        """ Draw our rectangle """
        vector_x = self.x0 - self.x1
        vector_y = self.y0 - self.y1
        length = math.sqrt(vector_x * vector_x + vector_y * vector_y)
        if length != 0:
            arcade.draw_line(self.x0, self.y0, self.x1, self.y1, color=arcade.color.RED, line_width=3)


class Tuss:
    def __init__(self, nSpan, nFloor):
        self.nodes, self.elements = create_bridge(20, 5)
        DoF = self.nodes.shape[0] * 2
        self.K, self.C, self.M = get_stiffness_mtx(self.nodes, self.elements)
        self.restricted_nodes = np.asarray([0, 1, 38, 39])
        self.actDof = np.setdiff1d(np.arange(DoF), self.restricted_nodes)
        self.K = self.K[np.ix_(self.actDof, self.actDof)]
        self.C = self.C[np.ix_(self.actDof, self.actDof)]
        self.M = self.M[np.ix_(self.actDof, self.actDof)]

        # self.A = scipy.linalg.inv(np.array(np.bmat([[np.zeros((DoF-4, DoF-4), dtype=np.float64), self.M],
        #                                [self.M, self.C]]), dtype=np.float64))
        # self.A = np.matmul(self.A, np.bmat([[-self.M, np.zeros((DoF-4,DoF-4))],
        #                                        [np.zeros((DoF-4,DoF-4)), self.K]]))
        #
        self.B = np.linalg.inv(np.array(np.bmat([[np.zeros((DoF - 4, DoF - 4), dtype=np.float64), self.M],
                                                  [self.M, self.C]]), dtype=np.float64))

        self.inv_M = scipy.linalg.inv(self.M)
        self.A = np.array(np.bmat([[-np.matmul(self.inv_M, self.C), -np.matmul(self.inv_M, self.K)],
                                                    [np.eye(DoF - 4), np.zeros((DoF - 4, DoF - 4), dtype=np.float64)]]), dtype=np.float64)

        eigen = scipy.linalg.eig(self.A)[0]
        print(eigen)
        print(np.abs(eigen*0.01+1))

        eigen = np.abs(eigen)
        print(np.max(eigen), np.min(eigen))
        eig = scipy.linalg.eig(self.B)[1]
        eig = np.abs(eig)
        print(np.max(eig), np.min(eig))

        self.P, self.L, self.U = scipy.linalg.lu(self.K)
        self.F = np.zeros((self.nodes.shape[0] * 2), dtype=np.float64)
        self.displacement = np.zeros((self.nodes.shape[0], 2), dtype=np.float64)
        self.velocity = np.zeros((self.nodes.shape[0]*2-4,1), dtype=np.float64)

        self.PL_inv = np.linalg.inv(np.matmul(self.P, self.L))
        self.U_inv = np.linalg.inv(self.U)

    def step(self,dt):
        h = 0.01
        for i in range(100):
            displacement = self.displacement.reshape(-1, 1)[self.actDof]
            velocity = self.velocity

            y_idx = [i for i in range(74 * 2 + 1, (74 + 16) * 2 + 1, 2)]
            f = np.zeros((self.nodes.shape[0] * 2, 1), dtype=np.float64)

            f[y_idx, :] = 0. # np.sin(i/100.*np.pi, dtype=np.float64) * 5000
            f = f[self.actDof,:]

            y = np.concatenate((velocity, displacement))

            dy = np.matmul(self.A, y) + np.concatenate((np.matmul(self.inv_M, f), np.zeros((self.nodes.shape[0] * 2-4,1))))
            dy = dy * h

            dv = dy[0:176]
            du = dy[176:352]

            U = np.zeros(self.nodes.shape[0] * 2)
            U[np.ix_(self.actDof)] = du[:,0]

            self.displacement += U.reshape(-1,2)
            self.velocity += dv

        print("fps", 1./dt)

    def update_load_F(self, fx, fy):
        scale = 5.
        x_idx = [i for i in range(74 * 2, (74 + 16) * 2, 2)]
        self.F[x_idx] = fx * scale
        y_idx = [i for i in range(74 * 2 + 1, (74 + 16) * 2 + 1, 2)]
        self.F[y_idx] = fy * scale
        y = np.matmul(self.PL_inv, np.matmul(self.P, self.F[self.actDof]))
        U_active = np.matmul(self.U_inv, y)

        U = np.zeros(self.nodes.shape[0] * 2)
        U[np.ix_(self.actDof)] = U_active
        self.displacement = U.reshape((-1, 2))

    def draw(self):
        no_elements = self.elements.shape[0]
        no_nodes = self.nodes.shape[0]

        elements = self.elements
        nodes = self.nodes + self.displacement

        scale = 60.
        shift = (10, 100)
        color = arcade.color.BLACK

        for i in range(no_elements):
            pt1 = elements[i][0]
            pt2 = elements[i][1]
            arcade.draw_line(nodes[pt1][0] * scale + shift[0], nodes[pt1][1] * scale + shift[1],
                             nodes[pt2][0] * scale + shift[0], nodes[pt2][1] * scale + shift[1], color, 2)


class MyGame(arcade.Window):
    """ Main application class. """

    def __init__(self, width, height):
        super().__init__(width, height, title="Keyboard control")
        arcade.set_background_color(arcade.color.WHITE)
        self.line = None
        self.tuss = None
        self.left_down = False

    def setup(self):
        """ Set up the game and initialize the variables. """
        self.line = Line(0, 0, 0, 0)
        self.tuss = Tuss(20, 5)
        self.left_down = False

    def update(self, dt):
        """ Move everything """
        if self.left_down is False:
            self.tuss.step(dt)

    def on_draw(self):
        """
        Render the screen.
        """
        arcade.start_render()

        self.tuss.draw()
        self.line.draw()

    def on_mouse_motion(self, x, y, dx, dy):
        """
        Called whenever the mouse moves.
        """
        if not self.left_down:
            self.line.x0 = x
            self.line.y0 = y
        else:
            self.tuss.update_load_F(self.line.x1 - self.line.x0, self.line.y1 - self.line.y0)

        self.line.x1 = x
        self.line.y1 = y

    def on_mouse_press(self, x, y, button, modifiers):
        """
        Called when the user presses a mouse button.
        """
        if button == arcade.MOUSE_BUTTON_LEFT:
            self.left_down = True

    def on_mouse_release(self, x, y, button, modifiers):
        """
        Called when a user releases a mouse button.
        """
        if button == arcade.MOUSE_BUTTON_LEFT:
            self.left_down = False


def main():
    window = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT)
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
