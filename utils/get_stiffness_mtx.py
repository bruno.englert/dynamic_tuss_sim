import numpy as np

from .get_rotation_mtx import get_rotation_mtx


def get_stiffness_mtx(nodes, elements, EA=5e5, rhoA=4000.):
    no_elements = elements.shape[0]
    K = np.zeros((nodes.shape[0] * 2, nodes.shape[0] * 2), dtype=np.float64)
    C = np.zeros((nodes.shape[0] * 2, nodes.shape[0] * 2), dtype=np.float64)
    M = np.zeros((nodes.shape[0] * 2, nodes.shape[0] * 2), dtype=np.float64)

    for i in range(no_elements):
        node1 = elements[i][0]
        node2 = elements[i][1]

        x = nodes[node1][0] - nodes[node2][0]
        y = nodes[node1][1] - nodes[node2][1]
        l = np.sqrt(x ** 2 + y ** 2)
        rotation_mtx = get_rotation_mtx(x, y)

        node1 = node1 * 2
        node2 = node2 * 2

        current_k = np.asarray([[1, -1], [-1, 1]], np.float64) * EA
        current_k = np.matmul(np.matmul(rotation_mtx.T, current_k), rotation_mtx) / l
        K[node1:node1 + 2, node1:node1 + 2] += current_k[0:2, 0:2]
        K[node1:node1 + 2, node2:node2 + 2] += current_k[0:2, 2:4]
        K[node2:node2 + 2, node1:node1 + 2] += current_k[2:4, 0:2]
        K[node2:node2 + 2, node2:node2 + 2] += current_k[2:4, 2:4]

        current_c = np.asarray([[1, -1], [-1, 1]], np.float64) * 8000.
        current_c = np.matmul(np.matmul(rotation_mtx.T, current_c), rotation_mtx) / l
        C[node1:node1 + 2, node1:node1 + 2] += current_c[0:2, 0:2]
        C[node1:node1 + 2, node2:node2 + 2] += current_c[0:2, 2:4]
        C[node2:node2 + 2, node1:node1 + 2] += current_c[2:4, 0:2]
        C[node2:node2 + 2, node2:node2 + 2] += current_c[2:4, 2:4]

        current_m = l * np.eye(4, dtype=np.float64) / 2. * rhoA
        M[node1:node1 + 2, node1:node1 + 2] += current_m[0:2, 0:2]
        M[node1:node1 + 2, node2:node2 + 2] += current_m[0:2, 2:4]
        M[node2:node2 + 2, node1:node1 + 2] += current_m[2:4, 0:2]
        M[node2:node2 + 2, node2:node2 + 2] += current_m[2:4, 2:4]

    return K, C, M
