import arcade


def plot_bridge(nodes, elements, scale, shift, color):
    no_elements = elements.shape[0]
    no_nodes = nodes.shape[0]

    for i in range(no_elements):
        pt1 = elements[i][0]
        pt2 = elements[i][1]
        arcade.draw_line(nodes[pt1][0]*scale+shift[0], nodes[pt1][1]*scale+shift[1],
                         nodes[pt2][0]*scale+shift[0], nodes[pt2][1]*scale+shift[1], color, 2)

    for i in range(no_nodes):
        arcade.draw_point(nodes[i][0]*scale+shift[0], nodes[i][1]*scale+shift[1], arcade.color.YELLOW, 4)
