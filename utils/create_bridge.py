import numpy as np


def create_bridge(nSpan, nFloor):
    nodes = []
    elements = []

    elemstart = 0
    for iFloor in range(nFloor):
        lSpan = nSpan - iFloor
        y0 = iFloor
        x0 = iFloor / 2.
        for i in range(lSpan):
            nodes.append([x0 + i, y0])
        if lSpan > 0:
            for i in range(0, lSpan-1):
                elements.append([elemstart + i, elemstart + i + 1])

        if iFloor < nFloor -1:
            for i in range(0, lSpan-1):
                elements.append([elemstart + i, elemstart + i + lSpan])
                elements.append([elemstart + i + 1, elemstart + lSpan + i])
        elemstart = elemstart + lSpan

    return np.asarray(nodes), np.asarray(elements)